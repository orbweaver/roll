use rand::*;
use regex::{Match, Regex};
use std::str::FromStr;
use structopt::StructOpt;

#[derive(StructOpt)]
struct Args {
    #[structopt(help = "Dice specifier, e.g. 'd6' or '3d8+10'")]
    dice: String,
    #[structopt(short = "s", long = "spellpower", default_value = "0")]
    spellpower: i32,
}

fn value_from_match(m: Option<Match>, default: i32) -> i32 {
    match m {
        Some(mtch) => i32::from_str(mtch.as_str()).unwrap_or(default),
        None => default,
    }
}

fn roll_dice(s: &str) -> Result<i32, &'static str> {
    // Parse dice specification
    let re = Regex::new(r"^(\d*)d(\d+)([+]\d+)?$").unwrap();
    let caps = re.captures(s).ok_or("failed to parse dice string")?;
    let n = value_from_match(caps.get(1), 1);
    let d = i32::from_str(caps.get(2).unwrap().as_str()).expect("failed to parse dice face count");
    let p = value_from_match(caps.get(3), 0);

    // Roll each dice and accumulate total
    let mut total = 0;
    let mut rng = rand::thread_rng();
    for _ in 0..n {
        total += rng.gen_range(1..(d + 1));
    }

    Ok(total + p)
}

fn main() {
    let args = Args::from_args();
    let scale_factor = 1.0 + (args.spellpower as f32 / 100.0);
    match roll_dice(&args.dice) {
        Ok(result) => println!("{}", (result as f32 * scale_factor) as i32),
        Err(err) => println!("ERROR: {}", err),
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    // Roll a dice string numerous times and return a hashmap of result frequencies
    fn accumulate_rolls(dice: &str) -> HashMap<i32, i32> {
        let mut results = HashMap::new();

        // In 500 rolls of a d6, the probability of missing one number is 2.6e-40
        // In 500 rolls of a d20, the probability of missing one number is 7.3e-12
        for _ in 1..500 {
            let roll = crate::roll_dice(dice).unwrap();
            let entry = results.entry(roll).or_insert(0);
            *entry += 1;
        }
        results
    }

    // Confirm range of values in map corresponds to given range
    fn map_covers_range(map: &HashMap<i32, i32>, low: i32, high: i32) -> Result<(), String> {
        // Confirm that hashmap contains at least one of each key in the given range
        for k in low..(high + 1) {
            if !map.contains_key(&k) {
                return Err(format!("missing value: {k}"));
            }
        }

        // Confirm that map does not contain any values outside the range
        for k in map.keys() {
            if *k < low || *k > high {
                return Err(format!("value outside of range: {k}"));
            }
        }

        Ok(())
    }

    #[test]
    fn handle_invalid_string() {
        let result = crate::roll_dice("blah");
        assert!(result.is_err());
    }

    #[test]
    fn roll_d6() {
        let result = crate::roll_dice("d6").unwrap();
        assert!(result >= 1);
        assert!(result <= 6);

        // Confirm that each value appears at least once
        let rolls = accumulate_rolls("d6");
        map_covers_range(&rolls, 1, 6).unwrap();
    }

    #[test]
    fn roll_1d6() {
        let result = crate::roll_dice("1d6").unwrap();
        assert!(result >= 1);
        assert!(result <= 6)
    }

    #[test]
    fn roll_2d8() {
        let result = crate::roll_dice("2d8").unwrap();
        assert!(result >= 2);
        assert!(result <= 16);

        // Confirm range of results
        let rolls = accumulate_rolls("2d8");
        map_covers_range(&rolls, 2, 16).unwrap();
    }

    #[test]
    fn roll_3d6_plus_20() {
        let result = crate::roll_dice("3d6+20").unwrap();
        assert!(result >= 23);
        assert!(result <= 38);

        let rolls = accumulate_rolls("3d6+20");
        map_covers_range(&rolls, 23, 38).unwrap();
    }
}
